
<!-- README.md is generated from README.Rmd. Please edit that file -->
<!-- date: "11 de Mayo de 2020" -->

# EMEP EEA Emission factors data-base

Access to the EMEP EEA emission factors tier 2.
<https://www.eea.europa.eu/publications/emep-eea-guidebook-2019>

### Installation

``` r
library(remotes) 
install_gitlab("ibarraespinosa/emep")
```

## Functions

-   ef
-   find_ef

``` r
ef()
```

``` r
find_ef("electricity")
       NFR                                 Sector      Table                   Type                                   Technology        Fuel Abatement Region Pollutant Value  Unit
2  1.A.1.a Public electricity and heat production Table_3-20 Tier 2 emission factor Stationary reciprocating Engines - gas-fired Natural gas             <NA>        Cr  0.05 mg/GJ
11 1.A.1.a Public electricity and heat production Table_3-20 Tier 2 emission factor Stationary reciprocating Engines - gas-fired Natural gas             <NA>       TSP     2  g/GJ
   CI_lower CI_upper            Reference
2      0.01     0.25 Nielsen et al., 2010
11     1.00     3.00          BUWAL, 2001
```

``` r
data("emep_act")
head(emep_act)
#>       NFR                                 Sector
#> 1 1.A.1.a Public electricity and heat production
#> 2 1.A.1.a Public electricity and heat production
#> 3 1.A.1.a Public electricity and heat production
#> 4 1.A.1.a Public electricity and heat production
#> 5 1.A.1.a Public electricity and heat production
#> 6 1.A.1.a Public electricity and heat production
#>                                     Technology          Fuel Abatement Region
#> 1 Stationary reciprocating Engines - gas-fired   Natural gas             <NA>
#> 2                                         <NA>     Hard Coal             <NA>
#> 3                                 Gas Turbines Gaseous Fuels             <NA>
#> 4                                         <NA>    Brown Coal             <NA>
#> 5    Large stationary CI reciprocating engines       Gas Oil             <NA>
#> 6                                 Gas Turbines       Gas Oil             <NA>
```
