#' Data.frame with activity from EEA emissions guidelines
#'
#' @format A data frame:
#' \describe{
#'   \item{NFR}{}
#'   \item{Sector}{}
#'   \item{Table}{}
#'   \item{Type}{}
#'   \item{Technology}{}
#'   \item{Fuel}{}
#'   \item{Abatement}{}
#'   \item{Region}{}
#'   \item{Pollutant}{}
#'   \item{Value}{}
#'   \item{Unit}{}
#'   \item{CI_lower}{}
#'   \item{CI_upper}{}
#'   \item{Reference}{}
##' }
#' @source EEA
#' @usage data(emep)
#' @docType data
"emep"
